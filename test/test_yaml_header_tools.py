# Tests for yaml_header_tools

from os.path import join, dirname
from yaml_header_tools import *

MDFILES = join(dirname(__file__), "mdfiles")

def test_standard_header():
    # Test loading from directory:
    d = get_header_from_file(join(MDFILES, "testcase_a"))
    assert len(d["responsible"]) == 2
    assert d["description"][0].startswith("Alternating")
    assert len(d["data"]) == 3
    assert d["tags"][0] == "unit test"
    assert d["tags"][-1] == "frequency sweep"
    assert d["empty_field"] == [""]

    # Test loading from directory, without cleaning the header:
    d = get_header_from_file(join(MDFILES, "testcase_a"), False)
    assert len(d["responsible"]) == 2
    assert d["description"].startswith("Alternating")
    assert len(d["data"]) == 3
    assert d["tags"][0] == "unit test"
    assert d["tags"][-1] == "frequency sweep"
    assert d["empty_field"] == ""

    # Test loading file directly:
    d = get_header_from_file(join(MDFILES, "testcase_a/README.md"), True)
    assert len(d["responsible"]) == 2
    assert len(d["data"]) == 3

def test_loading_order():
    # Test correct loading order of readme files in folder:
    d = get_header_from_file(join(MDFILES, "testcase_b"), False)
    assert len(d["responsible"]) == 2
    d = get_header_from_file(join(MDFILES, "testcase_b/readme.md"), False)
    assert len(d["responsible"]) == 1
               
